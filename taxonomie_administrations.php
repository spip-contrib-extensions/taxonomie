<?php
/**
 * Fichier gérant l'installation et la désinstallation du plugin Taxonomie.
 *
 * @package    SPIP\TAXONOMIE\CONFIGURATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Fonction d'installation et de mise à jour du plugin.
 * Le schéma du plugin est composé d'une table `spip_taxons` et d'une configuration.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin
 *                                      installé dans SPIP
 * @param string $version_cible         Version du schéma de données (déclaré dans paquet.xml)
 *
 * @return void
**/
function taxonomie_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	// Créer la configuration par défaut du plugin
	$config_defaut = configurer_taxonomie();

	// A l'installation du plugin
	$maj = [];
	$maj['create'] = [
		['maj_tables', ['spip_taxons']],
		['ecrire_config', 'taxonomie', $config_defaut]
	];

	// Ajout de la colonne cle_gbif : inutile de mettre à jour les espèces existantes
	$maj['2'] = [
		[
			'sql_alter',
			'TABLE spip_taxons ADD COLUMN cle_gbif bigint(21) DEFAULT 0 NOT NULL AFTER tsn_parent'
		],
		['sql_alter', 'TABLE spip_taxons ADD INDEX (cle_gbif)'],
	];

	// Ajout des colonnes IUCN : inutile de mettre à jour les espèces existantes
	$maj['3'] = [
		[
			'sql_alter',
			'TABLE spip_taxons ADD COLUMN statut_iucn varchar(5) DEFAULT "" NOT NULL AFTER cle_gbif'
		],
		[
			'sql_alter',
			'TABLE spip_taxons ADD COLUMN evaluation_iucn text NOT NULL AFTER statut_iucn'
		],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin.
 *
 * @param string $nom_meta_base_version Nom de la meta informant de la version du schéma de données du plugin
 *                                      installé dans SPIP.
 *
 * @return void
**/
function taxonomie_vider_tables(string $nom_meta_base_version) : void {
	// Supprimer la table des taxons créées par le plugin
	sql_drop_table('spip_taxons');

	// Nettoyer les versionnages
	sql_delete('spip_versions', sql_in('objet', ['taxon']));
	sql_delete('spip_versions_fragments', sql_in('objet', ['taxon']));

	// Effacer la meta de chaque règne chargé.
	include_spip('taxonomie_fonctions');
	foreach (regne_repertorier() as $_regne) {
		effacer_meta("taxonomie_{$_regne}");
	}

	// Effacer la meta de configuration du plugin
	effacer_meta('taxonomie');

	// Effacer la meta du schéma de la base
	effacer_meta($nom_meta_base_version);
}

/**
 * Initialise la configuration du plugin.
 *
 * @return array Le tableau de la configuration par défaut qui servira à initialiser la meta `taxonomie`.
 */
function configurer_taxonomie() : array {
	return [
		'langues_utilisees' => ['fr'],
		'services_utilises' => ['wikipedia', 'gbif'],
		'iucn_token'        => ''
	];
}
