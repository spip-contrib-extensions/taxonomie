<?php
/**
 * Ce fichier contient l'action `taxon_evaluer_iucn` qui permet de compléter la description d'une espèce
 * de son évaluation IUCN si elle existe.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Cette action permet de mettre à jour les champs statut_iucn et evaluation_iucn qui fournissent le détail
 * de l'état de conservation de l'espèce.
 *
 * Cette action est réservée aux utilisateurs ayant l'autorisation de modifier un taxon.
 * Elle nécessite un argument, l'id du taxon.
 *
 * @return void
 */
function action_taxon_evaluer_iucn_dist() : void {
	// Securisation et autorisation car c'est une action auteur:
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$id_taxon = $securiser_action();

	// Synchronisation du nom et du descriptif du site avec les données du site idoine de la boussole
	if ($id_taxon = (int) $id_taxon) {
		// Verification des autorisations
		if (!autoriser('modifier', 'taxon', $id_taxon)) {
			include_spip('inc/minipres');
			echo minipres();
			exit();
		}

		// On récupère le nom scientifique et le tsn du taxon pour l'appel de l'api IUCN
		include_spip('action/editer_objet');
		$taxon = objet_lire('taxon', $id_taxon, ['champs' => ['nom_scientifique', 'tsn', 'sources']]);

		// Récupération des informations de conservation et préparation du set de modifications
		// -- si l'évaluation n'est pas disponible on impose le statut non évalué
		$maj = [
			'statut_iucn'     => 'NE',
			'evaluation_iucn' => [
				'code'      => 'NE',
				'categorie' => 'ne'
			]
		];
		// -- on acquiert l'évaluation si elle existe
		include_spip('services/iucn/iucn_api');
		$search = [
			'name' => $taxon['nom_scientifique'],
			'tsn'  => $taxon['tsn']
		];
		if ($evaluation = iucn_get_assessment($search)) {
			// -- statut d'évaluation
			$maj['statut_iucn'] = $evaluation['code'] ?? '';
			// -- détails d'évaluation
			$maj['evaluation_iucn'] = $evaluation;
			// -- ajout de la source uniquement si l'évaluation est disponible
			$sources = unserialize($taxon['sources']);
			foreach (['statut_iucn', 'evaluation_iucn'] as $_champ) {
				if (
					empty($sources['iucn']['champs'])
					or !in_array($_champ, $sources['iucn']['champs'])
				) {
					$sources['iucn']['champs'][] = $_champ;
				}
			}
			$maj['sources'] = serialize($sources);
		}

		// Mise à jour du taxon pour signifier qu'on a bien accéder à IUCN
		$maj['evaluation_iucn'] = serialize($maj['evaluation_iucn']);
		objet_modifier('taxon', $id_taxon, $maj);
	}
}
