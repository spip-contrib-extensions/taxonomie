<?php
/**
 * Définit les autorisations du plugin Taxonomie.
 *
 * @package    SPIP\TAXONOMIE\TAXON
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline.
 *
 * @pipeline autoriser
**/
function taxonomie_autoriser() {
}

// -----------------
// taxons

/**
 * Autorisation de créer un taxon.
 * - Il faut être au moins rédacteur.
 *
 * @param string          $faire Action demandée : creer
 * @param string          $type  Type d'objet ou élément : taxon
 * @param null|int|string $id    Identifiant du taxon : inutilisé puisque création
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_taxon_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], ['0minirezo', '1comite']);
}

/**
 * Autorisation de modifier un taxon.
 * - il faut pouvoir en créer un
 * - et que l'id soit précisé et corresponde à celui d'un taxon existant.
 *
 * @param string          $faire Action demandée : modifier
 * @param string          $type  Type d'objet ou élément : object taxon
 * @param null|int|string $id    Identifiant du taxon
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_taxon_modifier_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = false;
	if ($id_taxon = (int) $id) {
		$autoriser = autoriser('creer', 'taxon', $id, $qui, $opt)
			and sql_countsel('spip_taxons', 'id_taxon=' . $id_taxon);
	}

	return $autoriser;
}

/**
 * Autorisation de supprimer un taxon
 * - aucun taxon ne peut être supprimé individuellement.
 *
 * @param string          $faire Action demandée : supprimer
 * @param string          $type  Type d'objet ou élément : taxon
 * @param null|int|string $id    Identifiant du taxon
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_taxon_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('niet');
}

/**
 * Autorisation de voir un taxon.
 * - tout le monde est autorisé.
 *
 * @param string          $faire Action demandée : voir
 * @param string          $type  Type d'objet ou élément : taxon
 * @param null|int|string $id    Identifiant du taxon
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_taxon_voir_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('ok');
}

/**
 * Autorisation d'iconifier un taxon.
 * - il faut pouvoir modifier le taxon
 * - sachant que seules les espèces et les taxons de rang inférieur possède un logo.
 *
 * @param string          $faire Action demandée : iconifier
 * @param string          $type  Type d'objet ou élément : taxon
 * @param null|int|string $id    Identifiant du taxon
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_taxon_iconifier_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = false;

	if (
		($id_taxon = (int) $id)
		and autoriser('modifier', 'taxon', $id, $qui, $opt)
	) {
		// On récupère le champ indiquant si le taxon est une espèce ou pas.
		$where = ["id_taxon={$id_taxon}"];
		$espece = sql_getfetsel('espece', 'spip_taxons', $where);
		if ($espece === 'oui') {
			$autoriser = true;
		}
	}

	return $autoriser;
}

/**
 * Autorisation de modifier le statut d'un taxon.
 * Cela n'est possible que :
 * - si l'auteur possède l'autorisation de modifier le taxon
 * - et le taxon est une espèce
 * - et que l'espèce est soit une feuille de la hiérarchie soit possède des enfants dont aucun n'est au statut
 *   publié.
 *
 * @param string          $faire Action demandée : instituer
 * @param string          $type  Type d'objet ou élément : taxon
 * @param null|int|string $id    Identifiant du taxon
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
 */
function autoriser_taxon_instituer_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = false;

	if ($id_taxon = (int) $id) {
		// On récupère les informations sur le taxon concerné et en particulier si celui-ci est bien une espèce
		// ou un descendant d'espèce.
		$from = 'spip_taxons';
		$where = ["id_taxon={$id_taxon}"];
		$select = ['espece', 'statut', 'tsn'];
		$taxon = sql_fetsel($select, $from, $where);

		// On teste d'abord si le statut demandé n'est pas le même que le statut du taxon.
		// C'est un cas rare qui peut arriver après un refus d'instituer et qui est bien sur autorisé.
		$statut_demande = $opt['statut'];
		if ($statut_demande == $taxon['statut']) {
			$autoriser = true;
		} elseif (($taxon['espece'] == 'oui') and autoriser('modifier', 'taxon', $id_taxon, $qui, $opt)) {
			// On vérifie que l'espèce ne possède pas des descendants directs
			$where = ['tsn_parent=' . (int) ($taxon['tsn'])];
			$select = ['statut'];
			$enfants = sql_allfetsel($select, $from, $where);
			if (!$enfants) {
				// Si le taxon est une feuille de la hiérarchie alors il peut toujours être institué.
				$autoriser = true;
			} else {
				// Le taxon a des descendants.
				// - si un descendants est publié alors l'espèce concernée l'est aussi et ne peut pas être
				//   instituée (prop ou poubelle) sous peine de casser la hiérarchie.
				// - si aucun descendant n'est publié alors quelque soit le statut de l'espèce concernée celle-ci
				//   peut être instituée.
				$enfants_statut = array_column($enfants, 'statut');
				if (($statut_demande == 'publie')
				or (($statut_demande == 'prop')
					and !in_array('publie', $enfants_statut))
				or (($statut_demande == 'poubelle')
					and !in_array('publie', $enfants_statut)
					and !in_array('prop', $enfants_statut))) {
					$autoriser = true;
				}
			}
		}
	}

	return $autoriser;
}

/**
 * Autorisation de voir la liste des taxons.
 * - tout le monde est autorisé.
 *
 * @param string          $faire Action demandée : voir
 * @param string          $type  Type d'objet ou élément : pas un objet mais _taxons pour indiquer la liste
 * @param null|int|string $id    Identifiant du taxon : inutilisé
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_taxons_voir_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('ok');
}

/**
 * Autorisation sur l'entrée de menu affichant la liste des taxons.
 * - même autorisation que `voir_taxons`, c'est-à-dire, tout le monde.
 *
 * @param string          $faire Action demandée : menu
 * @param string          $type  Type d'objet ou élément : pas un objet mais _taxons pour indiquer la liste
 * @param null|int|string $id    Identifiant du taxon : inutilisé
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_taxons_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('voir', '_taxons', $id, $qui, $opt);
}

// -----------------
// Objet especes

/**
 * Autorisation de voir un élément de menu, à savoir celui des espèces.
 * - tout le monde est autorisé.
 *
 * @param string          $faire Action demandée : menu
 * @param string          $type  Type d'objet ou élément : pas un objet mais _especes pour indiquer la liste
 * @param null|int|string $id    Identifiant du taxon : inutilisé
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_especes_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('ok');
}

/**
 * Autorisation de créer une espèce.
 * - il faut pouvoir créer un taxon (une espèce est un taxon particulier)
 * - et qu'un règne est au moins déjà chargé.
 *
 * @param string          $faire Action demandée : creer
 * @param string          $type  Type d'objet ou élément : pseudo objet espece
 * @param null|int|string $id    Identifiant du taxon : inutilisé puisque création
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_espece_creer_dist($faire, $type, $id, $qui, $opt) {
	// On vérifie qu'un règne est bien déjà chargé
	include_spip('taxonomie_fonctions');
	$regnes = regne_repertorier();

	// Il faut aussi être admin ou rédacteur.
	$autoriser = ($regnes and autoriser('creer', 'taxon', $id, $qui, $opt));

	return $autoriser;
}

/**
 * Autorisation de voir le bouton d'accès rapide de création d'une espèce.
 * - il faut pouvoir créer une espèce.
 *
 * @param string          $faire Action demandée : menu
 * @param string          $type  Type d'objet ou élément : élement de menu espececreer
 * @param null|int|string $id    Identifiant du taxon : inutilisé
 * @param array           $qui   Description de l'auteur demandant l'autorisation
 * @param array           $opt   Options de cette autorisation : inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_espececreer_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('creer', 'espece', null, $qui, $opt);
}
