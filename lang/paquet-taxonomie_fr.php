<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/taxonomie.git

return [

	// T
	'taxonomie_description' => 'Ce plugin fournit l’objet espèce et l’interface de gestion associée dans l’interface privée de SPIP.
	Chaque espèce est reliée à une arborescence taxonomique du règne au genre chargée à partir de la base de données internationale ITIS.
	Le plugin supporte les règnes animal, végétal et fongique.',
	'taxonomie_nom' => 'Taxonomie',
	'taxonomie_slogan' => 'Gérer les espèces du monde vivant',
];
