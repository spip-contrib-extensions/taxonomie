<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/taxonomie.git

return [

	// C
	'champ_auteur_label' => 'Auteur',
	'champ_cle_gbif_label' => 'Clé GBIF',
	'champ_descriptif_label' => 'Descriptif rapide',
	'champ_evaluation_iucn_label' => 'Détails de l’évaluation IUCN',
	'champ_nom_commun_label' => 'Nom commun',
	'champ_nom_scientifique_label' => 'Nom scientifique',
	'champ_rang_label' => 'Rang',
	'champ_regne_label' => 'Règne',
	'champ_sources_label' => 'Sources',
	'champ_statut_iucn_abr' => 'IUCN',
	'champ_statut_iucn_label' => 'Etat de conservation',
	'champ_texte_label' => 'Texte',
	'champ_tsn_label' => 'Numéro ITIS',
	'champ_tsn_parent_label' => 'Taxon parent',

	// I
	'icone_creer_taxon' => 'Créer une espèce',
	'icone_modifier_taxon' => 'Modifier ce taxon',
	'info_1_espece' => 'Une espèce ou taxon de rang inférieur',
	'info_1_taxon' => 'Un taxon du règne au genre',
	'info_aucun_taxon' => 'Aucun taxon du règne au genre',
	'info_aucune_espece' => 'Aucune espèce ou descendant',
	'info_nb_especes' => '@nb@ espèces et taxons de rang inférieur',
	'info_nb_taxons' => '@nb@ taxons du règne au genre',

	// R
	'role_territoire_taxon_endemic' => 'endémique',
	'role_territoire_taxon_extinct' => 'éteint',
	'role_territoire_taxon_introduced' => 'introduit',
	'role_territoire_taxon_reintroduced' => 'réintroduit',
	'role_territoire_taxon_resident' => 'originaire',

	// S
	'statut_iucn_cr_label' => 'en danger critique',
	'statut_iucn_dd_label' => 'données insuffisantes',
	'statut_iucn_e_label' => 'en danger',
	'statut_iucn_en_label' => 'en danger',
	'statut_iucn_ew_label' => 'éteinte à l\\état sauvage',
	'statut_iucn_ex_label' => 'éteinte',
	'statut_iucn_lc_label' => 'préoccupation mineure',
	'statut_iucn_lr_cd_label' => 'préoccupation mineure / dépendant de mesures de conservation',
	'statut_iucn_lr_lc_label' => 'préoccupation mineure',
	'statut_iucn_lr_nt_label' => 'quasi menacée',
	'statut_iucn_ne_label' => 'non évaluée',
	'statut_iucn_nr_label' => 'non reconnue',
	'statut_iucn_nt_label' => 'quasi menacée',
	'statut_iucn_pe_label' => 'peut-être éteinte',
	'statut_iucn_pew_label' => 'peut-être éteinte à l’état sauvage',
	'statut_iucn_t_label' => 'menacée',
	'statut_iucn_v_label' => 'vulnérable',
	'statut_iucn_vu_label' => 'vulnérable',

	// T
	'texte_ajouter_taxon' => 'Ajouter un taxon',
	'texte_changer_statut_taxon' => 'Ce taxon est :',
	'texte_statut_poubelle' => 'à la poubelle',
	'texte_statut_prop' => 'pour publication',
	'texte_statut_publie' => 'publié',
	'titre_espece' => 'Espèce',
	'titre_especes' => 'Espèces',
	'titre_logo_taxon' => 'Logo de ce taxon',
	'titre_taxon' => 'Taxon',
	'titre_taxons' => 'Taxons',
	'titre_taxons_rubrique' => 'Taxons de la rubrique',
];
