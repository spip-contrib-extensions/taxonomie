<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/taxon-taxonomie?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// C
	'champ_auteur_label' => 'Autor',
	'champ_cle_gbif_label' => 'Chave GBIF',
	'champ_descriptif_label' => 'Descrição',
	'champ_evaluation_iucn_label' => 'Detalhes da avaliação IUCN',
	'champ_nom_commun_label' => 'Nome comum',
	'champ_nom_scientifique_label' => 'Nome científico',
	'champ_rang_label' => 'Nível',
	'champ_regne_label' => 'Reino',
	'champ_sources_label' => 'Fontes',
	'champ_statut_iucn_abr' => 'IUCN',
	'champ_statut_iucn_label' => 'Estado de conservação',
	'champ_texte_label' => 'Texto',
	'champ_tsn_label' => 'Número ITIS',
	'champ_tsn_parent_label' => 'Téxon superior',

	// I
	'icone_creer_taxon' => 'Criar uma espécie',
	'icone_modifier_taxon' => 'Modificar este táxon',
	'info_1_espece' => 'Uma espécie ou táxon de nível inferior',
	'info_1_taxon' => 'Um táxon do reino ao gênero',
	'info_aucun_taxon' => 'Nenhum táxon do reino ao gênero',
	'info_aucune_espece' => 'Nenhuma espécie ou descendente',
	'info_nb_especes' => '@nb@ espécies e táxons de nível inferior',
	'info_nb_taxons' => '@nb@ táxons do reino ao gênero',

	// R
	'role_territoire_taxon_endemic' => 'endêmico',
	'role_territoire_taxon_extinct' => 'extinto',
	'role_territoire_taxon_introduced' => 'introduzido',
	'role_territoire_taxon_reintroduced' => 'reintroduzido',
	'role_territoire_taxon_resident' => 'originário',

	// S
	'statut_iucn_cr_label' => 'em perigo crítico',
	'statut_iucn_dd_label' => 'dados insuficientes',
	'statut_iucn_e_label' => 'em perigo',
	'statut_iucn_en_label' => 'em perigo',
	'statut_iucn_ew_label' => 'extinto em estado selvagem',
	'statut_iucn_ex_label' => 'extinto',
	'statut_iucn_lc_label' => 'preocupação menor',
	'statut_iucn_lr_cd_label' => 'preocupação menor / dependente de medidas de conservação',
	'statut_iucn_lr_lc_label' => 'preocupação menor',
	'statut_iucn_lr_nt_label' => 'quase ameaçado',
	'statut_iucn_ne_label' => 'não avaliado',
	'statut_iucn_nr_label' => 'não reconhecido',
	'statut_iucn_nt_label' => 'quase ameaçado',
	'statut_iucn_pe_label' => 'pode estar extinto',
	'statut_iucn_pew_label' => 'pode estar extindo em estado selvagem',
	'statut_iucn_t_label' => 'ameaçado',
	'statut_iucn_v_label' => 'vulnerável',
	'statut_iucn_vu_label' => 'vulnerável',

	// T
	'texte_ajouter_taxon' => 'Incluir um táxon',
	'texte_changer_statut_taxon' => 'Este táxon está:',
	'texte_statut_poubelle' => 'na lixeira',
	'texte_statut_prop' => 'para publicação',
	'texte_statut_publie' => 'publicado',
	'titre_espece' => 'Espécie',
	'titre_especes' => 'Espécies',
	'titre_logo_taxon' => 'Imagem deste táxon',
	'titre_taxon' => 'Táxon',
	'titre_taxons' => 'Táxons',
	'titre_taxons_rubrique' => 'Táxons da seção',
];
