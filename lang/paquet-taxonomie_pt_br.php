<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/taxonomie-paquet-xml-taxonomie?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// T
	'taxonomie_description' => 'Este plugin fornece o objeto {espécie} e a interface de gerenciamento associada na área restrita do SPIP.

Cada espécie é vinculada a uma hierarquia taxonômica {do reino ao gênero}, carregada a partir da base de dados internacional ITIS.

O plugin suporta os reinos animal, vegetal e fúngico.',
	'taxonomie_nom' => 'Taxonomia',
	'taxonomie_slogan' => 'Gerenciar as espécies do mundo vivo',
];
