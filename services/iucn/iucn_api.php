<?php
/**
 * Ce fichier contient l'ensemble des constantes et fonctions implémentant le service web de IUCN Red List.
 *
 * @package SPIP\TAXONOMIE\SERVICES\IUCN
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_TAXONOMIE_IUCN_ENDPOINT_BASE_URL')) {
	/**
	 * Préfixe des URL du service web de WIKIPEDIA.
	 */
	define('_TAXONOMIE_IUCN_ENDPOINT_BASE_URL', 'http://apiv3.iucnredlist.org/api/v3/');
}

if (!defined('_TAXONOMIE_IUCN_REDLIST')) {
	/**
	 * URL de base d'une page d'un taxon sur le site de la red list IUCN.
	 * Cette URL est fournie dans les credits.
	 */
	define('_TAXONOMIE_IUCN_REDLIST', 'https://www.iucnredlist.org/');
}

if (!defined('_TAXONOMIE_IUCN_SITE_URL')) {
	/**
	 * URL de la page d'accueil du site ITIS.
	 * Cette URL est fournie dans les credits.
	 */
	define('_TAXONOMIE_IUCN_SITE_URL', 'https://www.iucn.org/');
}

if (!defined('_TAXONOMIE_IUCN_CACHE_TIMEOUT')) {
	/**
	 * Période de renouvellement du cache de Wikipedia (6 mois).
	 */
	define('_TAXONOMIE_IUCN_CACHE_TIMEOUT', 86400 * 30 * 6);
}

$GLOBALS['iucn_categorie'] = [
	/**
	 * Configuration des catégories IUCN.
	 */
	// Extinct
	'EX' => ['item' => 'ex', 'versions' => ['3.1', '2.3']],
	// Extinct in the Wild
	'EW' => ['item' => 'ew', 'versions' => ['3.1', '2.3']],
	// Critically Endangered
	'CR' => ['item' => 'cr', 'versions' => ['3.1', '2.3']],
	// Critically Endangered (possibly extinct) - BirdLife
	'PE'  => ['item' => 'pe', 'versions' => []],
	'PEW' => ['item' => 'pew', 'versions' => []],
	// Endangered
	'EN' => ['item' => 'en', 'versions' => ['3.1', '2.3']],
	'E'  => ['item' => 'e', 'versions' => []],
	// Vulnerable
	'VU' => ['item' => 'vu', 'versions' => ['3.1', '2.3']],
	'V'  => ['item' => 'v', 'versions' => []],
	// Threatened
	'T' => ['item' => 't', 'versions' => []],
	// Lower Risk (conservation dependent)
	'LR/cd' => ['item' => 'lr_cd', 'versions' => ['2.3']],
	// Near Threatened
	'NT' => ['item' => 'nt', 'versions' => ['3.1']],
	// Lower Risk (near threatened)
	'LR/nt' => ['item' => 'lr_nt', 'versions' => ['2.3']],
	// Least Concern
	'LC' => ['item' => 'lc', 'versions' => ['3.1']],
	// Lower Risk (least concern)
	'LR/lc' => ['item' => 'lr_lc', 'versions' => ['2.3']],
	// Data Deficient
	'DD' => ['item' => 'dd', 'versions' => ['3.1', '2.3']],
	// Not Evaluated
	'NE' => ['item' => 'ne', 'versions' => ['3.1', '2.3']],
	// Not Recognized - BirdLife
	'NR' => ['item' => 'nr', 'versions' => []],
];

$GLOBALS['iucn_language'] = [
	/**
	 * Configuration de la correspondance entre langue IUCN et code de langue SPIP.
	 * La langue du service est l'index, le code SPIP est la valeur.
	 */
	'fre' => 'fr',
	'eng' => 'en',
	'spa' => 'es'
];

$GLOBALS['iucn_webservice'] = [
	/**
	 * Variable globale de configuration de l'api des actions du service web IUCN Red List.
	 */
	'species' => [
		'assessment' => [
			'list'  => 'result/0',
			'index' => [
				'code'             => 'category',
				'critere'          => 'criteria',
				'tendance'         => 'population_trend',
				'date_publication' => 'published_year',
				'date_evaluation'  => 'assessment_date',
				'evaluateur'       => 'assessor'
			],
		],
		'history' => [
			'list'  => 'result',
			'index' => [
				'annee'     => 'year',
				'categorie' => 'category',
				'code'      => 'code'
			],
		]
	]
];

// -----------------------------------------------------------------------
// ------------ API du web service IUCN - Actions principales ------------
// -----------------------------------------------------------------------

/**
 * Renvoie l'ensemble des informations sur un taxon désigné par son identifiant unique TSN.
 *
 * @api
 *
 * @uses cache_est_valide()
 * @uses iucn_build_url()
 * @uses inc_taxonomie_requeter_dist()
 * @uses cache_ecrire()
 * @uses cache_lire()
 *
 * @param array $search Tableau contenant le taxon à cherché sous une forme textuelle et numérique:
 *                      - `name` : chaine de recherche qui est en généralement le nom scientifique du taxon.
 *                      - `tsn`  : identifiant ITIS du taxon, le TSN. Etant donné que ce service s'utilise toujours sur un taxon
 *                      existant le TSN existe toujours. Il sert à créer le fichier cache.
 *
 * @return array Si le taxon est trouvé, le tableau renvoyé possède les index associatifs suivants:
 *               - `nom_scientifique`  : le nom scientifique complet du taxon tel qu'il doit être affiché (avec capitales).
 *               - `rang`              : le nom anglais du rang taxonomique du taxon
 *               - `regne`             : le nom scientifique du règne du taxon en minuscules
 */
function iucn_get_assessment(array $search) : array {
	$assessment = [];

	if (!empty($search['name'] and !empty($search['tsn']))) {
		// Construction des options permettant de nommer le fichier cache.
		// -- inutile de préciser la durée de conservation car on utilise la valeur par défaut à savoir 6 mois.
		include_spip('inc/ezcache_cache');
		$cache = [
			'service' => 'iucn',
			'action'  => 'assessment',
			'tsn'     => $search['tsn']
		];

		if ((!$file_cache = cache_est_valide('taxonomie', 'apirest', $cache))
		or (defined('_TAXONOMIE_CACHE_FORCER') ? _TAXONOMIE_CACHE_FORCER : false)) {
			// Construire l'URL de l'api sollicitée
			$url = iucn_build_url('species', 'assessment', $search['name']);

			// Acquisition des données spécifiées par l'url
			$requeter = charger_fonction('taxonomie_requeter', 'inc');
			$data = $requeter($url);

			// Récupération des informations choisies parmi l'enregistrement reçu à partir de la configuration
			// de l'action.
			$api = $GLOBALS['iucn_webservice']['species']['assessment'];
			include_spip('inc/filtres');
			$data = $api['list'] ? table_valeur($data, $api['list'], null) : $data;
			if (!empty($data)) {
				foreach ($api['index'] as $_destination => $_keys) {
					$element = $_keys ? table_valeur($data, $_keys, null) : $data;
					$assessment[$_destination] = is_string($element) ? trim($element) : $element;
				}

				// Ajout de la catégorie dans un format non abrégé permettant de calculer le libellé traduit.
				$assessment['categorie'] = isset($GLOBALS['iucn_categorie'][$assessment['code']])
					? $GLOBALS['iucn_categorie'][$assessment['code']]['item']
					: '';
			}

			// Mise en cache systématique pour gérer le cas où la page cherchée n'existe pas.
			cache_ecrire('taxonomie', 'apirest', $cache, $assessment);
		} else {
			// Lecture et désérialisation du cache
			$assessment = cache_lire('taxonomie', 'apirest', $file_cache);
		}
	}

	return $assessment;
}

// ---------------------------------------------------------------------
// ------------ API du web service IUCN - Fonctions annexes ------------
// ---------------------------------------------------------------------

/**
 * Renvoie la langue telle que le service Wikipedia la désigne à partir du code de langue
 * de SPIP.
 *
 * @api
 *
 * @param string $spip_language Code de langue de SPIP. Prend les valeurs `fr`, `en`, `es`, etc.
 *                              La variable globale `$iucn_language` définit le transcodage langue IUCNs vers code SPIP.
 *
 * @return string Langue au sens de IUCN - `fre`, `eng`, `spa` - ou chaine vide sinon.
 */
function iucn_find_language(string $spip_language) : string {
	if (!$language = array_search($spip_language, $GLOBALS['iucn_language'])) {
		$language = 'fre';
	}

	return $language;
}

/**
 * Construit la phrase de crédits précisant que les données fournies proviennent d'une page de Wikipedia.
 *
 * @api
 *
 * @param int   $id_taxon     Id du taxon nécessaire pour construire l'url de la page Wikipedia concernée.
 * @param array $informations Tableau des informations complémentaires sur la source. Pour IUCN ce tableau fourni le ou
 *                            les champs remplis avec IUCN.
 *
 * @return string Phrase de crédit.
 */
function iucn_credit(int $id_taxon, array $informations) : string {
	// On crée l'url du taxon sur le site GBIF
	$link_taxon = '<a href="' . _TAXONOMIE_IUCN_REDLIST . '" rel="noreferrer">IUCN Red List</a>';
	$link_site = '<a href="' . _TAXONOMIE_IUCN_SITE_URL . '" rel="noreferrer">IUCN</a>';

	// La liste des champs concernés (a priori le descriptif)
	include_spip('inc/taxonomie');
	$champs = isset($informations['champs'])
		? implode(', ', array_map('taxon_traduire_champ', $informations['champs']))
		: '';

	// On établit la citation
	$credit = _T(
		'taxonomie:credit_iucn',
		[
			'url_site'  => $link_site,
			'url_taxon' => $link_taxon,
			'champs'    => strtolower($champs)
		]
	);

	return $credit;
}

// ----------------------------------------------------------------
// ------------ Fonctions internes utilisées par l'API ------------
// ----------------------------------------------------------------

/**
 * Construit l'URL de la requête IUCN correspondant à la demande utilisateur.
 *
 * @internal
 *
 * @param string $group  Groupe d'actions du même type. Prend les valeurs:
 *                       - `species` : groupe des actions de recherche du TSN à partir du nom commun ou scientifique
 * @param string $action Nom de l'action du service IUCN. Les valeurs pour le groupe species sont, par exemple, `assessment`,
 *                       `commonname` et `history`.
 * @param string $key    Clé de recherche qui dépend de l'action demandée. Ce peut être le nom scientifique, le TSN, etc.
 *                       Cette clé doit être encodée si besoin par l'appelant.
 *
 * @return string L'URL de la requête au service
 */
function iucn_build_url(string $group, string $action, string $key) : string {
	// On récupère le token enregistré pour l'accès à l'API
	include_spip('inc/config');
	$token = lire_config('taxonomie/iucn_token', '');

	// Construire la partie standard de l'URL de l'api sollicitée
	$url = _TAXONOMIE_IUCN_ENDPOINT_BASE_URL
		. $group
		. '/' . ($action === 'assessment' ? $key : '')
		. '?token=' . $token;

	return $url;
}
