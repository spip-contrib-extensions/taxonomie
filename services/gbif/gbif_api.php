<?php
/**
 * Ce fichier contient l'ensemble des constantes et fonctions implémentant le service de taxonomie ITIS.
 *
 * @package SPIP\TAXONOMIE\SERVICES\ITIS
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_TAXONOMIE_GBIF_ENDPOINT_BASE_URL')) {
	/**
	 * Préfixe des URL du service web de GBIF.org.
	 */
	define('_TAXONOMIE_GBIF_ENDPOINT_BASE_URL', 'https://api.gbif.org/v1/');
}

if (!defined('_TAXONOMIE_GBIF_SITE_URL')) {
	/**
	 * URL de la page d'accueil du site ITIS.
	 * Cette URL est fournie dans les credits.
	 */
	define('_TAXONOMIE_GBIF_SITE_URL', 'https://www.gbif.org/fr/');
}

if (!defined('_TAXONOMIE_GBIF_CACHE_TIMEOUT')) {
	/**
	 * Période de renouvellement du cache de GBIF (6 mois).
	 */
	define('_TAXONOMIE_GBIF_CACHE_TIMEOUT', 86400 * 30 * 6);
}

$GLOBALS['gbif_language'] = [
	/**
	 * Variable globale de configuration de la correspondance entre langue GBIF
	 * et code de langue SPIP. La langue du service est l'index, le code SPIP est la valeur.
	 */
	'eng' => 'en',
	'fra' => 'fr',
	'deu' => 'de',
	'ita' => 'it',
	'por' => 'pt',
	'spa' => 'es',
];

// -----------------------------------------------------------------------
// ------------ API du web service GBIF - Actions principales ------------
// -----------------------------------------------------------------------

/**
 * Renvoie les noms vernaculaires dans l'ensemble des langages supportés par Taxonomie pour un taxon identifié par son
 * nom scientifique.
 *
 * @api
 *
 * @uses cache_est_valide()
 * @uses itis_build_url()
 * @uses inc_taxonomie_requeter_dist()
 * @uses cache_ecrire()
 * @uses cache_lire()
 *
 * @param array $search Tableau contenant le taxon à chercher:
 *                      - `taxon_key` : identifiant numérique unique GBIF.
 *                      - `tsn`       : identifiant ITIS du taxon, le TSN. Il sert uniquement à créer le fichier cache.
 *
 * @return array Tableau des noms vernaculaires indexés par le code spip du langage.
 */
function gbif_get_vernaculars(array $search) : array {
	$vernaculars = [];

	if ((int) ($search['tsn'])) {
		// Construction des options permettant de nommer le fichier cache.
		// -- inutile de préciser la durée de conservation car on utilise la valeur par défaut à savoir 6 mois.
		include_spip('inc/ezcache_cache');
		$cache = [
			'service' => 'gbif',
			'action'  => 'vernaculars',
			'tsn'     => $search['tsn']
		];

		if (!$file_cache = cache_est_valide('taxonomie', 'apirest', $cache)) {
			// Construire l'URL pour la requête de récupération des noms vernaculaires
			// -- paramètres du path
			$parametres_path = [
				'species',
				(string) ($search['taxon_key']),
				'vernacularNames'
			];
			// -- paramètres de la query
			$parametres_query = [];

			// On extrait les langues utiles uniquement (cad celles supportées par Taxonomie)
			do {
				// Acquisition des données spécifiées par l'url
				// -- construction url complète incluant éventuellement le paging
				$url = gbif_build_url($parametres_path, $parametres_query);
				// -- acuquisition des noms vernaculaires
				$requeter = charger_fonction('taxonomie_requeter', 'inc');
				$data = $requeter($url);

				if (
					empty($data['erreur'])
					and !empty($data['results'])
				) {
					foreach ($data['results'] as $_result) {
						// On retient les noms vernaculaires dont la langue est l'une de celles utilisées par Taxonomie
						// -- on consever le champ country si rempli pour identifier si c'est une déclinaison locale
						if (array_key_exists($_result['language'], $GLOBALS['gbif_language'])) {
							$vernaculars[$GLOBALS['gbif_language'][$_result['language']]][$_result['vernacularName']] = $_result['country'] ?? '';
						}
					}

					// Mise à jour de l'offset au cas où on en aurait besoin
					$parametres_query['offset'] = $data['offset'] + $data['limit'];
				}
			} while (
				isset($data['endOfRecords'])
				and !$data['endOfRecords']
			);

			// Mise en cache systématique pour gérer le cas où la page cherchée n'existe pas.
			cache_ecrire('taxonomie', 'apirest', $cache, $vernaculars);
		} else {
			// Lecture et désérialisation du cache
			$vernaculars = cache_lire('taxonomie', 'apirest', $file_cache);
		}
	}

	return $vernaculars;
}

/**
 * Renvoie l'identifiant unique GBIF nommé taxonKey à partir du nom scientique du taxon.
 *
 * @api
 *
 * @uses gbif_build_url()
 * @uses inc_taxonomie_requeter_dist()
 *
 * @param string $nom_scientifique Chaine de recherche représentant le nom scientifique du taxon.
 *
 * @return int Identifiant numérique unique du taxon ou 0 sinon.
 */
function gbif_get_taxonkey(string $nom_scientifique) : int {
	$taxon_key = 0;

	if ($nom_scientifique) {
		// Construire l'URL de la requête de récupération du taxonKey
		// -- paramètres du path
		$parametres_path = [
			'species',
			'match'
		];
		// -- paramètres de query
		$parametres_query = [
			'name' => rawurlencode($nom_scientifique),
		];
		// -- construction
		$url = gbif_build_url($parametres_path, $parametres_query);

		// Acquisition des données spécifiées par l'url
		$requeter = charger_fonction('taxonomie_requeter', 'inc');
		$data = $requeter($url);

		// On vérifie que le tableau est complet sinon on retourne un tableau vide
		if (
			empty($data['erreur'])
			and !empty($data['rank'])
			and (strtoupper($data['rank']) === 'SPECIES')
			and !empty($data['usageKey'])
		) {
			$taxon_key = (int) ($data['usageKey']);
		}
	}

	return $taxon_key;
}

// ---------------------------------------------------------------------
// ------------ API du web service ITIS - Fonctions annexes ------------
// ---------------------------------------------------------------------

/**
 * Renvoie la langue telle que le service ITIS la désigne à partir du code de langue
 * de SPIP.
 *
 * @api
 *
 * @param string $spip_language Code de langue de SPIP. Prend les valeurs `fr`, `en`, `es`, etc.
 *                              La variable globale `$GLOBALS['itis_language']` définit le transcodage langue ITIS vers code SPIP.
 *
 * @return string Langue au sens d'ITIS en minuscules - `french`, `english`, `spanish` - ou chaine vide sinon.
 */
function gbif_find_language(string $spip_language) : string {
	if (!$language = array_search($spip_language, $GLOBALS['itis_language'])) {
		$language = '';
	}

	return $language;
}

/**
 * Construit la phrase de crédits précisant que les données fournies proviennent de la base de données
 * d'ITIS.
 *
 * @api
 *
 * @param int   $id_taxon     Id du taxon nécessaire pour construire l'url de la page GBIF.
 * @param array $informations Tableau des informations complémentaires sur la source. Pour GBIF ce tableau est vide.
 *
 * @return string Phrase de crédit.
 */
function gbif_credit(int $id_taxon, array $informations) : string {
	// On recherche la clé GBIF du taxon afin de construire l'url vers sa page sur GBIF
	$taxon = sql_fetsel('cle_gbif, nom_scientifique', 'spip_taxons', 'id_taxon=' . sql_quote($id_taxon));

	// On crée l'url du taxon sur le site GBIF
	$url_taxon = _TAXONOMIE_GBIF_SITE_URL . 'species/' . $taxon['cle_gbif'];
	$link_taxon = '<a class="nom_scientifique" href="' . $url_taxon . '" rel="noreferrer">' . spip_ucfirst($taxon['nom_scientifique']) . '</a>';
	$link_site = '<a href="' . _TAXONOMIE_GBIF_SITE_URL . '" rel="noreferrer">GBIF</a>';

	// La liste des champs concernés (a priori le descriptif)
	include_spip('inc/taxonomie');
	$champs = isset($informations['champs'])
		? implode(', ', array_map('taxon_traduire_champ', $informations['champs']))
		: '';

	// On établit la citation
	$credit = _T(
		'taxonomie:credit_gbif',
		[
			'url_site'  => $link_site,
			'url_taxon' => $link_taxon,
			'champs'    => strtolower($champs)
		]
	);

	return $credit;
}

// ----------------------------------------------------------------
// ------------ Fonctions internes utilisées par l'API ------------
// ----------------------------------------------------------------

/**
 * Construit l'URL de la requête GBIF correspondant à la demande utilisateur.
 *
 * @internal
 *
 * @param array      $parametres_path  Liste des paramètres de path à accoler dans l'ordre fourni.
 * @param null|array $parametres_query Liste des paramètres de query à ajouter.
 *
 * @return string L'URL de la requête au service
 */
function gbif_build_url(array $parametres_path, ?array $parametres_query = []) : string {
	// Construire l'URL de l'api sollicitée
	// -- la partie path
	$url = _TAXONOMIE_GBIF_ENDPOINT_BASE_URL
		. implode('/', $parametres_path);

	// -- la partie query éventuelle qui se rajoute au token
	if ($parametres_query) {
		$query = '';
		foreach ($parametres_query as $_champ => $_valeur) {
			$query .= ($query ? '&' : '?')
				. $_champ
				. '='
				. $_valeur;
		}
		$url .= $query;
	}

	return $url;
}
