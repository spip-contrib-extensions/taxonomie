<?php
/**
 * Gestion du formulaire de mise à jour des noms communs d'un taxon à partir de GBIF.
 *
 * @package    SPIP\TAXONOMIE\TAXON
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données : le formulaire récupère une page wikipedia pour le descriptif du taxon.
 * Le formulaire propose une page par défaut mais aussi une liste d'autres pages qui matchent avec le taxon.
 *
 * @uses gbif_get_tokenkey()
 *
 * @param int      $id_taxon Id du taxon concerné.
 * @param null|int $cle_gbif Identifiant GBIF du taxon ou 0 si pas encore connu.
 *
 * @return array Tableau des données à charger par le formulaire (affichage). Aucune donnée chargée n'est un
 *               champ de saisie, celle-ci sont systématiquement remises à zéro.
 *               - `_langues` : tableau des noms de langue utilisables indexé par le code de langue SPIP (étape 1).
 */
function formulaires_nommer_taxon_charger(int $id_taxon, ?int $cle_gbif = 0) : array {
	// Initialisation du chargement.
	$valeurs = [
		'_noms_communs' => [],
		'_pays'         => [],
		'_explications' => [],
		'_codes_langue' => [],
		'editable'      => true
	];

	// Déterminer les noms communs déjà assignés au taxon
	$traductions = [];
	$select = [
		'nom_scientifique',
		'nom_commun',
		'tsn'
	];
	if ($taxon = sql_fetsel($select, 'spip_taxons', ['id_taxon=' . sql_quote($id_taxon)])) {
		$noms_communs = trim($taxon['nom_commun']);
		if (
			include_spip('inc/filtres')
			and function_exists('extraire_trads')
		) {
			// Filtre extraire_trads avant spip 4.2
			if (preg_match(_EXTRAIRE_MULTI, $noms_communs, $match)) {
				$noms_communs = trim($match[1]);
			}
			$traductions = extraire_trads($noms_communs);
		} else {
			// Depuis spip 4.2 il faut passer par le collecteur Multis
			// -- on crée un objet collecteur multis
			include_spip('src/Texte/Collecteur/Multis');
			$collecteurMultis = new Spip\Texte\Collecteur\Multis();
			// -- on collecte les 2 multi en reconstituant la balise et on extrait les traductions
			//    car on est sur que le contenu est non vide.
			$multis = $collecteurMultis->collecter($noms_communs);
			$traductions = $multis[0]['trads'];
		}
	}

	// Acquisition de la clé GBIF si celle-ci n'est pas encore connue
	include_spip('services/gbif/gbif_api');
	if (!$cle_gbif) {
		// On recherche la clé GBIF à partir du nom scientifique.
		if ($cle_gbif = gbif_get_taxonkey($taxon['nom_scientifique'])) {
			// De façon à éviter d'autres requêtes pour acquérir la clé on la stocke en base.
			include_spip('action/editer_objet');
			objet_modifier('taxon', (int) $id_taxon, ['cle_gbif' => $cle_gbif]);
		}
	}

	// Acquisition des noms communs si la clé GBIF a bien été trouvée
	$noms_communs_gbif = [];
	if ($cle_gbif) {
		$search = [
			'tsn'       => $taxon['tsn'],
			'taxon_key' => $cle_gbif
		];
		$noms_communs_gbif = gbif_get_vernaculars($search);

		// Présentation des noms communs GBIF en regard de leur langue et de l'existant éventuel.
		if ($noms_communs_gbif) {
			// On filtre les noms communs sur la liste des langues utilisées effectivement par le site.
			include_spip('inc/lang');
			include_spip('inc/config');
			$langues_utilisees = lire_config('taxonomie/langues_utilisees');
			foreach ($langues_utilisees as $_code_langue) {
				if (isset($noms_communs_gbif[$_code_langue])) {
					// On construit la liste des noms communs de la langue en excluant les noms communs déjà affectés
					foreach ($noms_communs_gbif[$_code_langue] as $_nom_commun => $_pays) {
						if (
							!isset($traductions[$_code_langue])
							or (strtolower($traductions[$_code_langue]) !== strtolower($_nom_commun))
						) {
							$valeurs['_noms_communs'][$_code_langue][$_nom_commun] = $_nom_commun;
							$valeurs['_pays'][$_code_langue][$_nom_commun] = $_pays;
						}
					}

					// On construit les explications qui rappelent les traductions déjà existantes pour le taxon
					$valeurs['_explications'][$_code_langue] = isset($traductions[$_code_langue])
						? _T('taxonomie:explication_gbif_nom_commun', ['nom' => $traductions[$_code_langue]])
						: _T('taxonomie:explication_gbif_nom_commun_aucun');

					// On construit la liste des codes de langue ayant au moins un nom commun pour l'affichage des fieldsets
					$valeurs['_codes_langue'][$_code_langue] = traduire_nom_langue($_code_langue);
				}
			}
		}
	}

	// Si aucun nom commun GBIF n'est disponible on envoie un message d'erreur.
	if (!$noms_communs_gbif) {
		$valeurs['editable'] = false;
	}

	return $valeurs;
}

/**
 * Vérification du formulaire : on doit au moins choisir un nom commun.
 *
 * @param int      $id_taxon Id du taxon concerné.
 * @param null|int $cle_gbif Identifiant GBIF du taxon ou 0 si pas encore connu.
 *
 * @return array Message d'erreur saisie obligatoire si aucun nom n'est choisi
 */
function formulaires_nommer_taxon_verifier(int $id_taxon, ?int $cle_gbif = 0) : array {
	// Initialisation des erreurs de vérification.
	$erreurs = [];

	// Récupération de la liste des langues ayant proposées des noms communs
	$langues = _request('_langues');
	if ($langues) {
		// On boucle sur chaque langue pour déterminer si l'utilisateur a choisi un nom commun et on constitue
		// le multi du nouveau nom commun à merger avec l'existant.
		$nom_est_choisi = false;
		foreach (explode(':', $langues) as $_code_langue) {
			$selection = _request("noms_communs_{$_code_langue}");
			if ($selection) {
				$nom_est_choisi = true;
				break;
			}
		}

		if (!$nom_est_choisi) {
			$erreurs['message_erreur'] = _T('taxonomie:erreur_saisie_nom_commun_obligatoire');
		}
	}

	return $erreurs;
}

/**
 * Exécution du formulaire : si une page est choisie et existe le descriptif est inséré dans le taxon concerné
 * et le formulaire renvoie sur la page d'édition du taxon.
 *
 * @uses wikipedia_get_page()
 * @uses convertisseur_texte_spip()
 * @uses taxon_merger_traductions()
 *
 * @param int      $id_taxon Id du taxon concerné.
 * @param null|int $cle_gbif Identifiant GBIF du taxon ou 0 si pas encore connu.
 *
 * @return array Tableau retourné par le formulaire contenant toujours un message de bonne exécution ou
 *               d'erreur. L'indicateur editable est toujours à vrai.
 */
function formulaires_nommer_taxon_traiter(int $id_taxon, ?int $cle_gbif = 0) : array {
	// Initialisation du retour et de la veraible permettant de connaitre l'état du traitement
	$retour = [];
	$nouveaux_noms = '';

	// Récupération de la liste des langues ayant proposées des noms communs
	$langues = _request('_langues');
	if ($langues) {
		// On boucle sur chaque langue pour déterminer si l'utilisateur a choisi un nom commun et on constitue
		// le multi du nouveau nom commun à merger avec l'existant.
		$nouveaux_noms = '';
		foreach (explode(':', $langues) as $_code_langue) {
			$selection = _request("noms_communs_{$_code_langue}");
			if ($selection) {
				$nouveaux_noms .= '[' . $_code_langue . ']' . $selection;
			}
		}

		if ($nouveaux_noms) {
			// On finalise le multi
			$nouveaux_noms = '<multi>' . $nouveaux_noms . '</multi>';

			// On acquiert le multi des noms communs existant
			$select = [
				'nom_commun',
				'sources'
			];
			$taxon = sql_fetsel($select, 'spip_taxons', ['id_taxon=' . sql_quote($id_taxon)]);

			// On merge les deux multi en considérant que les nouveaux sont prioritaires si une langue existe déjà
			// et on constitue le tableau des modifications du taxon
			include_spip('inc/taxonomie');
			$maj = [
				'nom_commun' => taxon_merger_traductions($nouveaux_noms, $taxon['nom_commun'])
			];

			// On ajoute la source GBIF si celle-ci n'est pas déjà présente
			$sources = unserialize($taxon['sources']);
			if (
				empty($sources['gbif']['champs'])
				or !in_array('nom_commun', $sources['gbif']['champs'])
			) {
				$sources['gbif']['champs'][] = 'nom_commun';
				$maj['sources'] = serialize($sources);
			}

			// On met à jour le taxon
			include_spip('action/editer_objet');
			objet_modifier('taxon', (int) $id_taxon, $maj);
		}
	}

	if (!$nouveaux_noms) {
		$retour['message_erreur'] = _T('taxonomie:erreur_maj_nom_commun');
	} else {
		// Redirection vers la page du taxon
		$retour['redirect'] = parametre_url(generer_url_ecrire('taxon'), 'id_taxon', $id_taxon);
	}

	return $retour;
}
