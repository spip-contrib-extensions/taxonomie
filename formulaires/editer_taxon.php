<?php
/**
 * Gestion du formulaire d'édition de l'objet taxon.
 *
 * @package SPIP\TAXONOMIE\TAXON
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité.
 *
 * @param null|int|string $id_taxon    Identifiant du taxon. 'new' pour un nouveau taxon.
 * @param null|string     $retour      URL de redirection après le traitement
 * @param null|int        $lier_trad   Identifiant éventuel d'un taxon source d'une traduction
 * @param null|string     $config_fonc Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param null|array      $row         Valeurs de la ligne SQL du taxon, si connu
 * @param null|string     $hidden      Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return string Hash du formulaire
 */
function formulaires_editer_taxon_identifier_dist($id_taxon = 'new', ?string $retour = '', ?int $lier_trad = 0, ?string $config_fonc = '', ?array $row = [], ?string $hidden = '') : string {
	return serialize([(int) $id_taxon]);
}

/**
 * Chargement du formulaire d'édition de taxon.
 * Déclarer les champs postés et y intégrer les valeurs par défaut.
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param null|int|string $id_taxon    Identifiant du taxon. 'new' pour un nouveau taxon.
 * @param null|string     $retour      URL de redirection après le traitement
 * @param null|int        $lier_trad   Identifiant éventuel d'un taxon source d'une traduction
 * @param null|string     $config_fonc Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param null|array      $row         Valeurs de la ligne SQL du taxon, si connu
 * @param null|string     $hidden      Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return array Environnement du formulaire.
 */
function formulaires_editer_taxon_charger_dist($id_taxon = 'new', ?string $retour = '', ?int $lier_trad = 0, ?string $config_fonc = '', ?array $row = [], ?string $hidden = '') : array {
	$valeurs = formulaires_editer_objet_charger('taxon', $id_taxon, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de taxon.
 * Vérifier les champs postés et signaler d'éventuelles erreurs.
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param null|int|string $id_taxon    Identifiant du taxon. 'new' pour un nouveau taxon.
 * @param null|string     $retour      URL de redirection après le traitement
 * @param null|int        $lier_trad   Identifiant éventuel d'un taxon source d'une traduction
 * @param null|string     $config_fonc Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param null|array      $row         Valeurs de la ligne SQL du taxon, si connu
 * @param null|string     $hidden      Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return array Tableau des erreurs indexé par le nom du champ en erreur.
 */
function formulaires_editer_taxon_verifier_dist($id_taxon = 'new', ?string $retour = '', ?int $lier_trad = 0, ?string $config_fonc = '', ?array $row = [], ?string $hidden = '') : array {
	return formulaires_editer_objet_verifier('taxon', $id_taxon);
}

/**
 * Traitement du formulaire d'édition de taxon.
 * Traiter les champs postés.
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param null|int|string $id_taxon    Identifiant du taxon. 'new' pour un nouveau taxon.
 * @param null|string     $retour      URL de redirection après le traitement
 * @param null|int        $lier_trad   Identifiant éventuel d'un taxon source d'une traduction
 * @param null|string     $config_fonc Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param null|array      $row         Valeurs de la ligne SQL du taxon, si connu
 * @param null|string     $hidden      Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 *
 * @return array Retours des traitements.
 */
function formulaires_editer_taxon_traiter_dist($id_taxon = 'new', ?string $retour = '', ?int $lier_trad = 0, ?string $config_fonc = '', ?array $row = [], ?string $hidden = '') : array {
	return formulaires_editer_objet_traiter('taxon', $id_taxon, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
}
