<?php
/**
 * Ce fichier contient les fonctions d'API du plugin Taxonomie utilisées comme filtre dans les squelettes.
 * Les autres fonctions de l'API sont dans le fichier `inc/taxonomie`.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fournit l'ascendance taxonomique d'un taxon donné, par consultation dans la base de données.
 *
 * @package SPIP\TAXONOMIE\TAXON
 *
 * @api
 *
 * @filtre
 *
 * @param int         $id_taxon   Id du taxon pour lequel il faut fournir l'ascendance.
 * @param null|int    $tsn_parent TSN du parent correspondant au taxon id_taxon. Ce paramètre permet d'optimiser le traitement
 *                                mais n'est pas obligatoire. Si il n'est pas connu lors de l'appel il faut passer `null`.
 * @param null|string $ordre      Classement de la liste des taxons : `descendant`(défaut) ou `ascendant`.
 *
 * @return array Liste des taxons ascendants. Chaque taxon est un tableau associatif contenant les informations
 *               suivantes : `id_taxon`, `tsn_parent`, `nom_scientifique`, `nom_commun`, `rang`, `statut` et l'indicateur
 *               d'espèce `espèce`.
 */
function taxon_informer_ascendance(int $id_taxon, ?int $tsn_parent = null, ?string $ordre = 'descendant') : array {
	$ascendance = [];

	// Si on ne passe pas le tsn du parent correspondant au taxon pour lequel on cherche l'ascendance
	// alors on le cherche en base de données.
	// Le fait de passer ce tsn parent est uniquement une optimisation.
	if (null === $tsn_parent) {
		$tsn_parent = sql_getfetsel('tsn_parent', 'spip_taxons', 'id_taxon=' . (int) $id_taxon);
	}

	while ($tsn_parent > 0) {
		$select = ['id_taxon', 'tsn_parent', 'nom_scientifique', 'nom_commun', 'rang_taxon', 'statut', 'espece'];
		$where = ['tsn=' . (int) $tsn_parent];
		$taxon = sql_fetsel($select, 'spip_taxons', $where);
		if ($taxon) {
			$ascendance[] = $taxon;
			$tsn_parent = $taxon['tsn_parent'];
		}
	}

	if ($ascendance and ($ordre == 'descendant')) {
		$ascendance = array_reverse($ascendance);
	}

	return $ascendance;
}

/**
 * Fournit les phrases de crédit des sources d'information ayant permis de compléter le taxon.
 * La référence ITIS n'est pas répétée dans le champ `sources` de chaque taxon car elle est
 * à la base de chaque règne. Elle est donc insérée par la fonction.
 *
 * @package SPIP\TAXONOMIE\TAXON
 *
 * @api
 *
 * @filtre
 *
 * @param int         $id_taxon            Id du taxon pour lequel il faut fournir les crédits
 * @param null|string $sources_specifiques Tableau sérialisé des sources possibles autres qu'ITIS (CINFO, WIKIPEDIA...)
 *                                         telles qu'enregistrées en base de données dans le champ `sources`.
 *                                         Ce paramètre permet d'optimiser le traitement mais n'est pas obligatoire.
 *
 * @return array Tableau des phrases de crédits indexées par source.
 */
function taxon_crediter(int $id_taxon, ?string $sources_specifiques = null) : array {
	$sources = [];

	// Si on ne passe pas les sources du taxon concerné alors on le cherche en base de données.
	// Le fait de passer ce champ sources est uniquement une optimisation.
	if (null === $sources_specifiques) {
		$sources_specifiques = sql_getfetsel('sources', 'spip_taxons', 'id_taxon=' . (int) $id_taxon);
	}

	// On merge ITIS et les autres sources
	$liste_sources = ['itis' => []];
	if ($sources_specifiques) {
		$liste_sources = array_merge($liste_sources, unserialize($sources_specifiques));
	}

	// Puis on construit la liste des sources pour l'affichage
	foreach ($liste_sources as $_service => $_infos_source) {
		include_spip("services/{$_service}/{$_service}_api");
		if (function_exists($citer = "{$_service}_credit")) {
			$sources[$_service] = $citer($id_taxon, $_infos_source);
		}
	}

	return $sources;
}

/**
 * Formate les éléments de l'évaluation IUCN pour un affichage.
 * Renvoie un tableau vide si le taxon n'a pas été encore évalué.
 *
 * @package SPIP\TAXONOMIE\TAXON
 *
 * @api
 *
 * @filtre
 *
 * @param array $evaluation Tableau des éléments de l'évaluation
 *
 * @return array Le tableau formaté ou vide.
 */
function taxon_formater_evaluation_iucn($evaluation) : array {
	$evaluation_iucn = [];

	if (
		$evaluation
		and is_array($evaluation)
		and ($evaluation['categorie'] !== 'ne')
	) {
		// Le statut et son code
		$evaluation_iucn[] =
			spip_ucfirst(_T('taxon:statut_iucn_' . $evaluation['categorie'] . '_label'))
			. ' (' . $evaluation['code'] . ')';
		// Le critère
		$evaluation_iucn[] = $evaluation['critere'];
		// La tendance
		$evaluation_iucn[] = spip_ucfirst(_T('taxonomie:info_tendance_iucn_' . strtolower($evaluation['tendance'])));
		// La date et l'évaluateur
		$options = [
			'date' => $evaluation['date_evaluation'],
			'nom'  => $evaluation['evaluateur']
		];
		$evaluation_iucn[] = _T('taxonomie:info_evaluation_iucn', $options);
		// la date de publication
		$options = [
			'date' => $evaluation['date_publication'],
		];
		$evaluation_iucn[] = _T('taxonomie:info_publication_iucn', $options);
	}

	return $evaluation_iucn;
}

/**
 * Renvoie la liste des règnes effectivement chargés en base de données.
 *
 * @package SPIP\TAXONOMIE\REGNE
 *
 * @api
 *
 * @filtre
 *
 * @return array Liste des noms scientifiques (en minuscules) des règnes chargés ou tableau vide.
 */
function regne_repertorier() : array {
	// Initialisation de la liste.
	static $regnes = null;

	if ($regnes === null) {
		include_spip('inc/taxonomie');
		$regnes = [];
		foreach (regne_lister_defaut() as $_regne) {
			if (regne_existe($_regne, $meta_regne)) {
				$regnes[] = $_regne;
			}
		}
	}

	return $regnes;
}
